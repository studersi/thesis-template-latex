####################################################################################################
# Build LaTeX projects
####################################################################################################

#---------------------------------------------------------------------------------------------------
# config (no file endings)
#---------------------------------------------------------------------------------------------------
PROJECT_NAME=main
BIB_FILE=bibtex
ACCRO_FILE=chapters/acronyms

#---------------------------------------------------------------------------------------------------
# build targets
#---------------------------------------------------------------------------------------------------
full: ${PROJECT_NAME}.pdf

${PROJECT_NAME}.pdf: clean typeset gloss accro
	latexmk -pdf -g

typeset: ${PROJECT_NAME}.tex
	latexmk -pdf

${PROJECT_NAME}.ist: typeset

${PROJECT_NAME}.glo: typeset

${PROJECT_NAME}.acn: typeset

gloss: ${PROJECT_NAME}.ist ${PROJECT_NAME}.glo ${ACCRO_FILE}.tex
	makeindex -s ${PROJECT_NAME}.ist \
		-t ${PROJECT_NAME}.glg \
		-o ${PROJECT_NAME}.gls \
		${PROJECT_NAME}.glo

accro: ${PROJECT_NAME}.ist ${PROJECT_NAME}.acn ${ACCRO_FILE}.tex
	makeindex -s ${PROJECT_NAME}.ist \
		-t ${PROJECT_NAME}.alg \
		-o ${PROJECT_NAME}.acr \
		${PROJECT_NAME}.acn

clean:
	find . -type f \( \
		-name "*.acn" \
		-o -name "*.acr" \
		-o -name "*.alg" \
		-o -name "*.aux" \
		-o -name "*.bbl" \
		-o -name "*.blg" \
		-o -name "*.dvi" \
		-o -name "*.fdb_latexmk" \
		-o -name "*.fls" \
		-o -name "*.glg" \
		-o -name "*.glo" \
		-o -name "*.gls" \
		-o -name "*.glsdefs" \
		-o -name "*.ist" \
		-o -name "*.lof" \
		-o -name "*.log" \
		-o -name "*.lol" \
		-o -name "*.lot" \
		-o -name "*.mw" \
		-o -name "*.nav" \
		-o -name "*.out" \
		-o -name "*.run.xml" \
		-o -name "*.toc" \
		-o -name "*.snm" \
		-o -name "*.nav" \
		-o -name "*.synctex.gz" \
		-o -name "*-blx.bib" \
	\) -exec rm -rf {} \;

clean-all: clean
	find . -type f -name ${PROJECT_NAME}.pdf -exec rm -rf {} \;
