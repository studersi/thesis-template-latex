Thesis Template
===============

Authors
-------
* Laura Rettig
* Simon Studer

Description
-----------
LaTeX template for writing a thesis and create presentation slides.

Requirements (Linux)
--------------------
* LaTeX package:  
`apt install texlive-full`

Usage
-----
* Build complete project and generate PDF:  
`make`
* Only do typesetting, don't update bibliography etc:  
`make typeset`
* Remove temporary files, except the generated PDF file:  
`make clean`
* Remove all temporary files:  
`make clean-all`